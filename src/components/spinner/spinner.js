import React from 'react';

import './spinner.css';

const Spinner = () => {
  return (
    <div className="loadingio-spinner-double-ring-qdy2ba2pud"><div className="ldio-8zeeu93lv1o">
<div></div>
<div></div>
<div><div></div></div>
<div><div></div></div>
</div></div>
  );
};

export default Spinner;
