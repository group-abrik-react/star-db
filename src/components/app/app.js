import React, { Component } from 'react';
import SwapiService from '../../services/swapi-service';
import DummySwapiService from '../../services/dummy-swapi-service';
import Header from '../header';
import RandomPlanet from '../random-planet';

import './app.css';
import ErrorBoundry from '../error-boundry';
import { SwapiServiceProvider } from '../swapi-service-context';

import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import planetDetails from '../sw-components/planet-details';
import { StarshipDetails } from '../sw-components';

import { 
    PeoplePage, 
    PlanetPage, 
    StarshipPage,
    LoginPage,
    SecretPage } from '../pages';


export default class App extends Component {
    state = {
        swapiService: new SwapiService(),
        isLoggedIn: false
    };

    onLogin = ()=>{
        this.setState({
            isLoggedIn: true
        });
    }

    onServiceChange = () => {
        this.setState(({ swapiService }) => {

            const Service = swapiService instanceof SwapiService ?
                DummySwapiService : SwapiService;

            return {
                swapiService: new Service()
            };
        });
    };

    render() {
        const {isLoggedIn} = this.state;
        return (
            <ErrorBoundry>
                <SwapiServiceProvider value={this.state.swapiService}>
                    <Router>
                        <div className="stardb-app">
                            <Header />
                            <RandomPlanet />
                            <Switch>
                                <Route path="/" render={() => <h2>Welcome to Star DB</h2>} exact />
                                <Route path="/people/:id?" component={PeoplePage} />
                                <Route path="/planet" component={PlanetPage} />
                                <Route path="/starship" component={StarshipPage} exact />
                                <Route path="/starship/:id"
                                    render={({ match }) => {
                                            const { id } = match.params;
                                            return <StarshipDetails itemId={id}/>
                                        }} />
                                <Route 
                                    path="/login" 
                                    render={() => (
                                        <LoginPage 
                                            isLoggedIn={isLoggedIn}
                                            onLogin={this.onLogin}
                                        />
                                    )} />
                                <Route 
                                    path="/secret" 
                                    render={() =>(
                                        <SecretPage isLoggedIn={isLoggedIn}/>
                                    )}/>
                                
                                <Route render={()=><h2>Page not found</h2>}/>    
                                <Redirect to="/"/>    
                            </Switch>
                        </div>
                    </Router>
                </SwapiServiceProvider>
            </ErrorBoundry>
        );
    }
};
