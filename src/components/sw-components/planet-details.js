import React from 'react';
import ItemDetails, { Record } from '../item-details/item-details';
import { withSwapiService } from '../hoc-helpers';


const PlanetDetails = (props) => {
    return (
        <ItemDetails {...props}>
            <Record field="pooulation" label="Population" />
            <Record field="rotationPeriod" label="Rotation period" />
            <Record field="diametr" label="Diametr" />
        </ItemDetails>
    )
}
const mapMethodsToProps = (swapiService) => {
    return {
        getData: swapiService.getPlanet,
        getImageUrl: swapiService.getPlanetImage
    }
};

export default withSwapiService(PlanetDetails,mapMethodsToProps);