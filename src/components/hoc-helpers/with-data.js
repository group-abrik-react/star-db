import React, { Component } from 'react';
import Spinner from "../spinner";

const withData = (View) => {
  return class extends Component {
    state = {
      data: null,
      loading: true
    };

    componentDidUpdate(prevProps) {
      if (this.props.getData !== prevProps.getData) {
        this.update();
      }
    }
    componentDidMount() {
      this.update();
    }

    update() {
      this.props.getData()
        .then((data) => {
          this.setState({
            data,
            loading: false
          });
        })
        .catch(() => {
          console.log("cathch");
          this.setState({
            error: true,
            loading: false
          })

        })
    }

    render() {
      const { data, loading } = this.state;

      if (loading) {
        return <Spinner />;
      }
      return <View {... this.props} data={data} />;
    }
  };
}

export default withData;